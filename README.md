# Flow-Sim

A simplified flow-based model for TCP LoLa including FFBquick.

Corresponding research paper currently under submission.

To be used with "Jupyter Notebook" (https://jupyter.org/)
